package ejerciciosHerencia;

import java.util.Arrays;

public abstract class electrodomesticos {
	private double precioBase=100;
	private String [] color= {"BLANCO","NEGRO","ROJO","AZUL","GRIS"};
	private String [] consumo= {"A","B","C","D","E","F"};
	private double peso=5;
	
	public electrodomesticos(double precioBase, String[] color, String[] consumo, double peso) {
		super();
		this.precioBase = precioBase;
		this.color = color;
		this.consumo = consumo;
		this.peso = peso;
	}
	public double getPrecioBase() {
		return precioBase;
	}
	public void setPrecioBase(double precioBase) {
		this.precioBase = precioBase;
	}
	public String[] getColor() {
		return color;
	}
	public void setColor(String[] color) {
		this.color = color;
	}
	public String[] getConsumo() {
		return consumo;
	}
	public void setConsumo(String[] consumo) {
		this.consumo = consumo;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	@Override
	public String toString() {
		return "electrodomesticos [precioBase=" + precioBase + ", color=" + Arrays.toString(color) + ", consumo="
				+ Arrays.toString(consumo) + ", peso=" + peso + "]";
	}
	public electrodomesticos(double precioBase, double peso) {
		super();
		this.precioBase = precioBase;
		this.peso = peso;
	}

}
