package ejerciciosHerencia;

public class revistas extends publicaciones{

	private int numero;
	private int mes;
	private int dia;
	
	@Override
	public String toString() {
		return "revistas [numero=" + numero + ", mes=" + mes + ", dia=" + dia + "]";
	}
	
	public revistas(int numero, int mes, int dia) {
		super();
		this.numero = numero;
		this.mes = mes;
		this.dia = dia;
	}

	public revistas() {
		super();
		// TODO Auto-generated constructor stub
	}

	public revistas(String codigo, String titulo, int anio, int numero, int mes, int dia) {
		super(codigo, titulo, anio);
		// TODO Auto-generated constructor stub
	}

	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	@Override
	public void prestamo() {
		
		
	}
}
