package ejerciciosHerencia;

public class libros extends publicaciones{
	
	private String autor;

	@Override
	public String toString() {
		return "libros [autor=" + autor + "]";
	}

	
	public libros() {
		super();
		// TODO Auto-generated constructor stub
	}


	public libros(String codigo, String titulo, int anio, String autor) {
		super(codigo, titulo, anio);
		
	}


	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	

	@Override
	public void prestamo() {
		// TODO Auto-generated method stub
		
	}
}
