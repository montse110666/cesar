package clases;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class LLENARvECTOR {
	static Scanner tc=new Scanner(System.in);

	public static void main(String[] args) {
		
		int tama�o=leerTama�o();
		
		ArrayList<Integer> lista=new ArrayList<>(tama�o);
		Random aleatorio=new Random();
		lista=llenarvector(tama�o,aleatorio);
	    System.out.println(lista);
	       
	}
	
	private static ArrayList<Integer> llenarvector(int taman, Random aleator) {
		ArrayList<Integer> lista1=new ArrayList<>();
		for (int i=0; i<taman; i++) {
			lista1.add(aleator.nextInt(201)-100);
			
		}
		return lista1;
	}

	private static int leerTama�o() {
		int taman;
		System.out.println("Introduce n�mero de elementos de la lista entre 10 y 200: ");
		taman=tc.nextInt();
		while (taman<10 || taman>200){
		   System.out.println("N�mero de elementos inv�lido");
		   System.out.println("Introduce n�mero de elementos de la lista entre 10 y 200: ");
		   taman=tc.nextInt();
	    }
		return taman;
   }
}
