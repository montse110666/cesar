package clases;

public class NoPerecederos extends productos{
    String tipo;    
    
	
    

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public NoPerecederos(String nombre, String codigo, double precio, String tipo) {
		super(nombre, codigo, precio);
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "NoPerecederos [tipo=" + tipo + "]";
	}
    public double calcular(int cantidad){
        
        return cantidad * getPrecio();
}
}
