package clases;

import java.util.Scanner;

public class coche {

	static Scanner tc=new Scanner(System.in);
		
		private String marca;
		private String matricula;
		private Motor motor;
		
		public String getMarca() {
			return marca;
		}

		public void setMarca(String marca) {
			this.marca = marca;
		}

		public String getMatricula() {
			return matricula;
		}

		public void setMatricula(String matricula) {
			this.matricula = matricula;
		}

		public Motor getMotor() {
			return motor;
		}

		public void setMotor(Motor motor) {
			this.motor = motor;
		}

		public coche(String marca, String matricula, Motor motor) {
			super();
			this.marca = marca;
			this.matricula = matricula;
			this.motor = motor;
		}

		public coche() {
			super();
		}

		@Override
		public String toString() {
			return "coche [marca=" + marca + ", matricula=" + matricula + ", motor=" + motor + "]";
		}
			
	}
