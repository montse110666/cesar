package clases;

public class Perecederos extends productos{

	int diasCaduc;
	
	public int getDiasCaduc() {
		return diasCaduc;
	}

	public void setDiasCaduc(int diasCaduc) {
		this.diasCaduc = diasCaduc;
	}

	@Override
	public String toString() {
		return "Perecederos [diasCaduc=" + diasCaduc + "]";
	}

	public Perecederos(String nombre, String codigo, double precio, int diasCaduc) {
		super(nombre, codigo, precio);
		this.diasCaduc = diasCaduc;
	}
	 public double calcular(int cantidad){
		 double [] valores = {4,3.5,3,2.5,2};
		 double preciototal=cantidad*getPrecio();
		 double preciofinal=preciototal-(preciototal*valores[getDiasCaduc()-1]/100);
	     return preciofinal;
	}
}
