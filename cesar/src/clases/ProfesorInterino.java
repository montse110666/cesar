package clases;

public class ProfesorInterino extends Profesor {

	private double complemento;
	
	
		

	public ProfesorInterino(String IdProfesor, String nombre, String apellidos, double sueldoBase) {
		super(IdProfesor, nombre, apellidos, sueldoBase);
		this.complemento = 600.00;
	}



	@Override
	public String toString() {
		return super.toString()+"complemento: "+complemento;
	}


	public double getComplemento() {
		return complemento;
	}


	public void setComplemento(int complemento) {
		this.complemento = complemento;
	}

	public  double importeNomina() {
		double importe=0;
		importe = super.getSueldoBase()+complemento;
		return importe;
	}
	
}
