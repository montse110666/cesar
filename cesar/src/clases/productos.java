package clases;

import java.io.Serializable;

public class productos implements Serializable {

	String nombre;
	String codigo;
	double precio;
	
	@Override
	public String toString() {
		return "productos [nombre=" + nombre + ", codigo=" + codigo + ", precio=" + precio + "]";
	}
	public productos(String nombre, String codigo, double precio) {
		super();
		this.nombre = nombre;
		this.codigo = codigo;
		this.precio = precio;
	}
	public productos() {
		super();
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
}
