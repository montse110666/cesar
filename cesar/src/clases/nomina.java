package clases;

import java.io.Serializable;
import java.util.ArrayList;

public class nomina implements Serializable{

	
		private String idprofesor;
		private double nomina;
		
		@Override
		public String toString() {
			return "nomina [idprofesor=" + idprofesor + ", nomina=" + nomina + "]";
		}

		public String getIdprofesor() {
			return idprofesor;
		}

		public void setIdprofesor(String idprofesor) {
			this.idprofesor = idprofesor;
		}

		public double getNomina() {
			return nomina;
		}

		public void setNomina(double nomina) {
			this.nomina = nomina;
		}

		public nomina(String idprofesor, double nomina) {
			super();
			this.idprofesor = idprofesor;
			this.nomina = nomina;
		}
		
	}

	

