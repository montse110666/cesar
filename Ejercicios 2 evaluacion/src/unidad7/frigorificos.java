package unidad7;

public class frigorificos extends Electrodomesticos {
	private boolean nofrost;

	public frigorificos() {
		super();
		nofrost=false;
	}
	public frigorificos(int preciobase, double peso,boolean nofrost) {
		super(preciobase, peso);
		this.nofrost = nofrost;
	}
	public frigorificos(int preciobase, String[] color, String consumo, double peso,boolean nofrost) {
		super(preciobase,color,consumo,peso);
		this.nofrost = nofrost;
	}
	public boolean isNofrost() {
		return nofrost;
	}
	public void setNofrost(boolean nofrost) {
		this.nofrost = nofrost;
	}
	@Override
	public String toString() {
		String imprime=super.toString();
		imprime=imprime+ "nofrost " + nofrost;
		return imprime;
	}
	
}
