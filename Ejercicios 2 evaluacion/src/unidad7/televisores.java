package unidad7;

public class televisores extends Electrodomesticos{
	private int tama�o=20;
	private String sintonizador="DVB-T";
	
	
	public televisores() {
		super();
		tama�o=20;
	}

	public televisores(int preciobase, double peso, int tama�o, String sintonizador) {
		super(preciobase, peso);
		this.tama�o = tama�o;
		this.sintonizador = sintonizador;
	}

	public televisores(int preciobase, String[] color, String consumo, double peso, int tama�o, String sintonizador) {
		super(preciobase,color,consumo,peso);
		this.tama�o = tama�o;
		this.sintonizador = sintonizador;
	}

	public int getTama�o() {
		return tama�o;
	}

	public void setTama�o(int tama�o) {
		this.tama�o = tama�o;
	}

	public String getSintonizador() {
		return sintonizador;
	}

	public void setSintonizador(String sintonizador) {
		this.sintonizador = sintonizador;
	}

	@Override
	public String toString() {
		String imprime=super.toString();
		imprime=imprime + "tama�o " + tama�o + "sintonizador " + sintonizador;
		return imprime;
	}
	
	
	
	

}
