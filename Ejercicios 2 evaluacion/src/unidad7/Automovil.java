package unidad7;

public class Automovil {
	
	private String modelo;
	private int capacidadDep;
	private int cantidadComb;
	private int consumo;
	private int kilometros;
	private int combustible;
	private int litros=0;
	
	public Automovil() {
		capacidadDep=0;
		cantidadComb=0;
		consumo=0;
		kilometros=0;
		combustible=0;
		
	}

	public Automovil(String modelo, int capacidadDep, int cantidadComb, int consumo) {
	
		this.modelo = modelo;
		this.capacidadDep = capacidadDep;
		this.cantidadComb = cantidadComb;
		this.consumo = consumo;
	}

	public Automovil(String modelo, int capacidadDep, int consumo) {
		super();
		this.modelo = modelo;
		this.capacidadDep = capacidadDep;
		this.consumo = consumo;
	}
	
	public int llenardeposito() {
	    litros=0;
		while (litros<capacidadDep) {
			litros++;
		}
	cantidadComb=litros;
	return cantidadComb;
}
	
	public int llenardeposito(int cantlitros){
		litros=0;
		int sobrante=0;
		while (litros<cantlitros) {
			litros++;
		}
		
		if (litros>capacidadDep) {
			cantidadComb=capacidadDep;
			sobrante=litros-capacidadDep;
		}
		return sobrante;
	}
	public int desplazar(int kmrecorrer) {
		if (consumo*kmrecorrer>cantidadComb) {
			System.out.println("No hay suficiente combustible");
		}
			else {
				cantidadComb=cantidadComb-consumo*kmrecorrer;
			}
		return cantidadComb;
		}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getCapacidadDep() {
		return capacidadDep;
	}

	public void setCapacidadDep(int capacidadDep) {
		this.capacidadDep = capacidadDep;
	}

	public int getCantidadComb() {
		return cantidadComb;
	}

	public void setCantidadComb(int cantidadComb) {
		this.cantidadComb = cantidadComb;
	}

	public int getConsumo() {
		return consumo;
	}

	public void setConsumo(int consumo) {
		this.consumo = consumo;
	}

	public int getCombustible() {
		return combustible;
	}

	public void setCombustible(int combustible) {
		this.combustible = combustible;
	}
	
	public void leerdatos() {
		System.out.println("Dame el modelo: ");
		
	}
	}




