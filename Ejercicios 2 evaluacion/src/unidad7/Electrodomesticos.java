package unidad7;

import java.util.Arrays;

public class Electrodomesticos {

	private int preciobase=100;
	private String color[]={"BLANCO","NEGRO","ROJO","AZUL","GRIS"};
	private String consumo;
	private double peso=5;
	private int preciofinal=preciobase;
	
		
	public Electrodomesticos() {
		super();
		
	}


	public Electrodomesticos(int preciobase, double peso) {
		super();
		this.preciobase = preciobase;
		this.peso = peso;
	}


	public Electrodomesticos(int preciobase, String[] color, String consumo, double peso) {
		super();
		this.preciobase = preciobase;
		this.color = color;
		this.consumo = consumo;
		this.peso = peso;
	}


	public int getPreciobase() {
		return preciobase;
	}




	public void setPreciobase(int preciobase) {
		this.preciobase = preciobase;
	}




	public String[] getColor() {
		return color;
	}




	public void setColor(String[] color) {
		this.color = color;
	}


	public String getConsumo() {
		return consumo;
	}




	public void setConsumo(String consumo) {
		this.consumo = consumo;
	}




	public double getPeso() {
		return peso;
	}




	public void setPeso(double peso) {
		this.peso = peso;
	}




	public void CalculaPrecioFinalConsumo() {
	
		switch(consumo) {
			
		case "A":
			preciofinal=preciofinal+preciofinal*30/100;
		case "B":
			preciofinal=preciofinal+preciofinal*25/100;
		case "C":
			preciofinal=preciofinal+preciofinal*20/100;
		case "D":
			preciofinal=preciofinal+preciofinal*15/100;
		case "E":
			preciofinal=preciofinal+preciofinal*10/100;
		case "F":
			preciofinal=preciofinal+preciofinal*5/100;
		}
		
	}
	public void CalculaPrecioFinalPeso() {
		if ((peso>0) & (peso<20)) {
			preciofinal=preciofinal+preciofinal*5/100;
		}
		else {
			if((peso>19) & (peso<50)) {
				preciofinal=preciofinal+preciofinal*10/100;
			}
			else {
				if((peso>49) & (peso<80)) {
					preciofinal=preciofinal+preciofinal*15/100;
				}
				else {
					if(peso>80) {
						preciofinal=preciofinal+preciofinal*20/100;
					}
				}
			}
		}
	}




	@Override
	public String toString() {
		return "Electrodomesticos [preciobase=" + preciobase + ", color=" + Arrays.toString(color) + ", consumo="
				+ consumo + ", peso=" + peso + ", preciofinal=" + preciofinal + "]";
	}
	
}
