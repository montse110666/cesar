package examen2eva;

public abstract class VehiculosTPer extends Vehiculos{

	private int plazas;

	@Override
	public String toString() {
		return "plazas=" + plazas;
	}

	public int getPlazas() {
		return plazas;
	}

	public void setPlazas(int plazas) {
		this.plazas = plazas;
	}

	public VehiculosTPer(String matricula, int plazas) {
		super(matricula);
		this.plazas = plazas;
	}
}
