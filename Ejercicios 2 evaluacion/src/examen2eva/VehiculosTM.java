package examen2eva;

public abstract class VehiculosTM extends Vehiculos {
	
	private int pma;
	
	public VehiculosTM(String matricula, int pma) {
		super(matricula);
		this.pma = pma;
	}

	public int getPma() {
		return pma;
	}

	public void setPma(int pma) {
		this.pma = pma;
	}

	@Override
	public String toString() {
		return "VehiculosTM [pma=" + pma + "]";
	}

	@Override
	public double getPrecioAlquiler(int dias) {
		
		return super.getPrecioAlquiler(dias)+20*pma;
		
	}
}
