package examen2eva;

public final class Coches extends VehiculosTPer {

	public Coches(String matricula, int plazas) {
		super(matricula, plazas);
		// TODO Auto-generated constructor stub
	}



	@Override
	public String toString() {
		return "Coches " + super.toString() + ", plazas=" + getPlazas() + ", matricula="
				+ getMatricula();
	}

	

	@Override
	public double getPrecioAlquiler(int dias) {
		return super.getPrecioAlquiler(dias)+1.5*getPlazas();
		
	}

	

}
