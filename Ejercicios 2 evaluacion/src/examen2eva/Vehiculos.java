package examen2eva;

public abstract class Vehiculos implements Alquilable{
	
	private String matricula;
	
	
	public Vehiculos() {
		super();
	}
	public Vehiculos(String matricula) {
		super();
		this.matricula = matricula;
	}
	@Override
	public String toString() {
		return "matricula=" + matricula;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public double getPrecioAlquiler(int dias) {
		double precio=50*dias;
		return precio;
	}
}
