package unidad5;

import java.util.ArrayList;
import java.util.Scanner;

import unidad5.Televisores.Sintonizador;


     
public class TiendaElectrodomesticos {
	static Scanner tc=new Scanner(System.in);

	public static void main(String[] args) {
		
		ArrayList<Electrodomesticos> electrodomesticos=new ArrayList<>();
		Electrodomesticos electro=new Electrodomesticos();
		Lavadoras lavadora=new Lavadoras();
		Frigorifico frigorifico=new Frigorifico();
		Televisores televisor=new Televisores();
		
		pideDatos(electrodomesticos,electro,lavadora,frigorifico,televisor);

	}

	private static void pideDatos(ArrayList<Electrodomesticos> electrodomesticos,Electrodomesticos electro,Lavadoras lavadora,
			Frigorifico frigorifico,Televisores televisor) {
		int opcion=0;
		do {
			System.out.println("Escoge Electrodom�stico:");
			System.out.println("1- Lavadora");
			System.out.println("2- Frigor�fico");	
			System.out.println("3- televisor");	
			opcion=tc.nextInt();
		}		
		while ((opcion<1) || (opcion>3));
		
		System.out.println("Escoge color: (BLANCO, NEGRO, ROJO, AZUL ,GRIS)");
		String color=tc.next().toUpperCase();
		Colores colores=Enum.valueOf(Colores.class,color);
		System.out.println("cual es su consumo? (A,B,C,D,E,F)");
		String consumo=tc.next().toUpperCase();
		Consumo consumEnum=Enum.valueOf(Consumo.class, consumo.toUpperCase());
		System.out.println("cual es su peso? ");
		int peso=tc.nextInt();
		double precioTotal=Electrodomesticos.CalculaPrecioFinal(consumEnum,peso);
		System.out.println("El precio seg�n consumo" +consumEnum+" Es "+precioTotal);
		System.out.println("-------------------------------------------------------------------");
		
		switch (opcion) {
			case 1:
				System.out.println("Introduce la carga de la lavadora: ");
				int kilos=tc.nextInt();
				double precioLavadora=Lavadoras.CalculaPrecioFinal(kilos, precioTotal);
				precioTotal=precioLavadora;
				lavadora.setColor(colores);
				lavadora.setConsumo(consumEnum);
				lavadora.setPeso(peso);
				lavadora.setKilos(kilos);
				lavadora.setPrecioFinal(precioTotal);
				electrodomesticos.add(lavadora);
				System.out.println("El precio de la lavadora es: "+ precioTotal);
				break;
			case 2:
				boolean NoFrost=false;
				System.out.println("Tiene el frigor�fico capacidad No-Frost? (s/n): ");
				String frost=tc.next().toUpperCase();
				if (frost.equals("S")) {
					NoFrost=true;
				}
				frigorifico.setColor(colores);
				frigorifico.setConsumo(consumEnum);
				frigorifico.setPeso(peso);
				frigorifico.setNoFrost(NoFrost);
				frigorifico.setPrecioFinal(precioTotal);
				electrodomesticos.add(frigorifico);
				System.out.println("El precio del frigor�fico es: "+ precioTotal);
				break;					
			case 3:
				int tama�o=0;				
				System.out.println("De cuantas pulgadas es el televisor? ");
			    tama�o=tc.nextInt();
			    System.out.println("Que tipo de sintonizador lleva el televisor? ");
			    String sintonizador=tc.next();
			    Sintonizador sintoEnum=Enum.valueOf(Sintonizador.class, sintonizador);
			    televisor.setColor(colores);
			    televisor.setConsumo(consumEnum);
			    televisor.setTama�o(tama�o);
			    televisor.setDvb(sintoEnum);
			    televisor.setPrecioFinal(precioTotal);
			    electrodomesticos.add(televisor);
			    System.out.println("El precio del televisor es: "+ precioTotal);
				break;

			default:
				System.out.println("opci�n no v�lida");
				break;
			}
		
			
			System.out.println(electrodomesticos);
	}

}
