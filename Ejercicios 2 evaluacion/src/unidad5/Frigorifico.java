package unidad5;

public class Frigorifico extends Electrodomesticos{

	@Override
	public String toString() {
		return "Frigorifico [noFrost=" + noFrost + "]";
	}

	public Frigorifico() {
		super();
	}

	public Frigorifico(double precioBase, Colores color, Consumo consumo, double peso, double precioFinal) {
		super(precioBase, color, consumo, peso, precioFinal);
	}

	public boolean isNoFrost() {
		return noFrost;
	}

	public void setNoFrost(boolean noFrost) {
		this.noFrost = noFrost;
	}

	

	private boolean noFrost=false;
}
