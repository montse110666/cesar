package unidad5;

import java.util.ArrayList;
import java.util.Collection;

public class Libro {
	
	private String titulo;
	private Collection<Autor> autores;
	private float precio;
	private int stock;
	
	public void Autor(Collection autores) {
		Collection Autor=new ArrayList<String>();
		Autor.add("Lorca");
		Autor.add("Garc�a Marquez");
		Autor.add("Oscar Wilde");
	}
	public Libro() {
		
	}
	
	public Libro(String titulo, Collection<Autor> autores, float precio) {
		
		this.autores=autores;
		this.titulo=titulo;
		this.precio=precio;
		stock=0;
				
	}
	public Libro (String titulo, Collection<Autor> autores, float precio, int stock) {
		
		this(titulo,autores,precio);
		this.stock=stock;
	}

	public String getTitulo() {
		return titulo;
	}

	
	public Collection<Autor> getAutores() {
		return autores;
	}

	

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String toString() {
		
		return "t�tulo: " + titulo + "Autor: " + autores + 
"precio " + precio + "�" + "Unidades en Stock: " + stock;
}
	
}
