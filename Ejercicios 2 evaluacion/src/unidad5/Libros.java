package unidad5;

public class Libros extends Publicaciones{

	private String autor;

	


	@Override
	public String toString() {
		return "\n LIBRO \n"+super.toString() + "\n autor=" + autor;
	}

	public Libros() {
		super();
	}

	public Libros(String codigo, String titulo, String a�o,String autor) {
		super(codigo, titulo, a�o);
		this.autor = autor;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}
}
