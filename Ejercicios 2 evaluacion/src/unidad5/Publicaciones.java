package unidad5;

public class Publicaciones {
	
	private String codigo;
	private String titulo;
	private String a�o;
	
	
	@Override
	public String toString() {
		return "codigo=" + codigo + "\n titulo=" + titulo + "\n a�o=" + a�o ;
	}
	public Publicaciones() {
		super();
	}
	public Publicaciones(String codigo, String titulo, String a�o) {
		super();
		this.codigo = codigo;
		this.titulo = titulo;
		this.a�o = a�o;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getA�o() {
		return a�o;
	}
	public void setA�o(String a�o) {
		this.a�o = a�o;
	}
	

}
