package unidad5;

import java.time.LocalDate;

public class Animal {

	String nombre;
	LocalDate fecha;
	
	public Animal() {
		nombre="Luna";
		
	}
	
	public Animal (String nombre, LocalDate fecha) {
		
		this.nombre=nombre;
		this.fecha=fecha;
	}
	
	public Animal (String nombre) {
		
		this.nombre=nombre;
		fecha=LocalDate.now();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	public String toString() {
		
	
		return "Nombre: "+ nombre +"Edad: 3 a�os";
		
		
	}
}
