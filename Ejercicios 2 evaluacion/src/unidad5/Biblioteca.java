package unidad5;

import java.util.ArrayList;
import java.util.Scanner;


public class Biblioteca {
	static Scanner tc=new Scanner(System.in);

	public static void main(String[] args) {
		
		ArrayList <Publicaciones> biblio=new ArrayList<>();
		Publicaciones publi1=new Publicaciones();
		Revistas rev1=new Revistas();
		Libros lib1=new Libros();
		pedirDatos(biblio,rev1,publi1,lib1);		

	}

	

	private static void pedirDatos(ArrayList<Publicaciones> biblio, Revistas rev1,  Publicaciones publi1, Libros lib1) {
		String opcion="s";
		do {
		System.out.println("Revista o Libro? R/L");
		String publi=tc.next();
		System.out.println("Introduce el c�digo: ");
		String codigo=tc.next();
		System.out.println("Introduce el t�tulo: ");
		String titulo=tc.next();
		System.out.println("Introduce el a�o: ");
		String a�o=tc.next();
		if (publi.equalsIgnoreCase("R")) {
			System.out.println("Introduce el n�mero: ");
			int numero=tc.nextInt();	
			System.out.println("Introduce el mes: ");
			String mes=tc.next();
			System.out.println("Introduce el dia: ");
			int dia=tc.nextInt();
			rev1.setCodigo(codigo);
			rev1.setTitulo(titulo);
			rev1.setA�o(a�o);
			rev1.setNumero(numero);
			rev1.setMes(mes);
			rev1.setDia(dia);
			biblio.add(rev1);
		}
		if (publi.equalsIgnoreCase("L")) {
			System.out.println("Introduce el autor: ");
			String autor=tc.next();
			lib1.setCodigo(codigo);
			lib1.setTitulo(titulo);
			lib1.setA�o(a�o);
			lib1.setAutor(autor);
			biblio.add(lib1);
			
		}
		System.out.println("Quieres introducir otra publicaci�n? (s/n)");
		opcion=tc.next();
		}
		while (opcion.equalsIgnoreCase("s"));
		System.out.println(biblio.toString());
		
	}

}
