package unidad5;

public class Electrodomesticos {
	
	private static double precioBase=100.00;
	private static Consumo consumo=Consumo.F;
	private static double peso=5;
	private static double precioFinal;
	
	public Electrodomesticos(double precioBase, Colores color, Consumo consumo, double peso, double precioFinal) {
		super();
		this.precioBase = precioBase;
		this.color = color;
		this.consumo = consumo;
		this.peso = peso;
		this.precioFinal = precioFinal;
	}
	public void setPrecioBase(double precioBase) {
		this.precioBase = precioBase;
	}
	private Colores color=Colores.BLANCO;
	public Colores getColor() {
		return color;
	}
	public void setColor(Colores color) {
		this.color = color;
	}
	public Consumo getConsumo() {
		return consumo;
	}
	public void setConsumo(Consumo consumo) {
		this.consumo = consumo;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public double getPrecioFinal() {
		return precioFinal;
	}
	public void setPrecioFinal(double precioFinal) {
		this.precioFinal = precioFinal;
	}
	public double getPrecioBase() {
		return precioBase;
	}
	
	
	public Electrodomesticos() {
		super();
	}
	public Electrodomesticos(Colores color, Consumo consumo, double peso, double precioFinal) {
		super();
		this.color = color;
		this.consumo = consumo;
		this.peso = peso;
		this.precioFinal = precioFinal;
	}
	@Override
	public String toString() {
		return "Electrodomesticos [precioBase=" + precioBase + ", peso=" + peso + "]";
	}
	
	public static double CalculaPrecioFinal(Consumo consumEnum, int peso2Consumo) {
		
		switch (consumo) {
		case A:
			precioFinal=precioBase+precioBase*30/100;
			break;
		case B:
			precioFinal=precioBase+precioBase*25/100;
			break;
		case C:
			precioFinal=precioBase+precioBase*20/100;
			break;
		case D:
			precioFinal=precioBase+precioBase*15/100;
			break;
			
		case E:
			precioFinal=precioBase+precioBase*10/100;
			break;
		case F:
			precioFinal=precioBase+precioBase*5/100;
			break;
		}
		if ((peso>-1) && (peso<20)) {
			precioFinal=precioFinal+precioFinal*5/100;				
		}
		if ((peso>19) && (peso<50)) {
			precioFinal=precioFinal+precioFinal*10/100;	
			
		}
		if ((peso>49) && (peso<80)) {
			precioFinal=precioFinal+precioFinal*15/100;	
			
		}
		if (peso>79) {
			precioFinal=precioFinal+precioFinal*20/100;	
			
		}
		
		
		
		return precioFinal;
	}
	
}

