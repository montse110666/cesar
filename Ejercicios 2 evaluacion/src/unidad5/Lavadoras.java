package unidad5;

public class Lavadoras extends Electrodomesticos {
	
	
	private int kilos;

	
	
	@Override
	public String toString() {
		return "Lavadoras [kilos=" + kilos + "]";
	}

	public Lavadoras() {
		super();
	}

	public Lavadoras(double precioBase, Colores color, Consumo consumo, double peso, double precioFinal, int kilos) {
		super(precioBase, color, consumo, peso, precioFinal);
		this.kilos = kilos;
	}

	public int getKilos() {
		return kilos;
	}

	public void setKilos(int kilos) {
		this.kilos = kilos;
	}

	public Lavadoras(double precioBase, Colores color, Consumo consumo, double peso, double precioFinal) {
		super(precioBase, color, consumo, peso, precioFinal);
		
	}
	
	public static double CalculaPrecioFinal(int pesoLav, double precioTotal) {
		
		if ((pesoLav==10) || (pesoLav==11)|| (pesoLav==13)) {
			precioTotal=precioTotal+precioTotal*10/100;
		}
		
		
		return precioTotal;
		
	}
}
	
	
	
