package unidad5;

public class Revistas extends Publicaciones{

	private int numero;
	private String mes;
	private int dia;
	
	
	@Override
	public String toString() {
		return "\n REVISTA \n"+super.toString()+"\n numero=" + numero + "\n mes=" + mes + "\n dia=" + dia;
	}
	public Revistas() {
		super();
	}
	public Revistas(String codigo, String titulo, String a�o, int numero, String mes, int dia) {
		super(codigo, titulo, a�o);
		this.numero = numero;
		this.mes = mes;
		this.dia = dia;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	
}
