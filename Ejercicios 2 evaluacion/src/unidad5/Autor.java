package unidad5;

import java.util.Scanner;

public class Autor {

	private String nombre;
	private String email;
	private String genero;
	
	public Autor() {
		nombre="vacio";
		email="vacio";
		genero="vacio";
	}
	
	
	public Autor(String nombre, String email, String genero) {
		
		this.nombre=nombre;
		this.email=email;
		this.genero=genero;
		
	}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getGenero() {
			return genero;
		}

		public void setGenero(String genero) {
			this.genero = genero;
		}
	
		public String toString() {
			
			return nombre +" " + genero + " " +  email;
		}
		
		public void leer() {
			
			Scanner entrada=new Scanner(System.in);
			System.out.println("Dame el nombre de Autor ");
			nombre=entrada.nextLine();
			System.out.println("Dame el emaile de Autor ");
			email=entrada.nextLine();
			System.out.println("El autor es de g�nero masculino o femenino  ");
			genero=entrada.nextLine();
		}
}

