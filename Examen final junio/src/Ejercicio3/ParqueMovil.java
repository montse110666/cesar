package Ejercicio3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;



public class ParqueMovil implements Serializable{
	
	private int a�o;
	private int total;

	public ParqueMovil(int a�o, int total) {
		super();
		this.a�o = a�o;
		this.total = total;
	}

	public int getA�o() {
		return a�o;
	}

	public void setA�o(int a�o) {
		this.a�o = a�o;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	Map<String,Map<String,Integer>> parque=new TreeMap<>();
	Map<String,Integer> mapa=new TreeMap<>();
	
	
	public ParqueMovil(String path, int a�o) {
		
		File file=new File(path+"\\pm"+a�o+".csv");
		
		try (BufferedReader fl=new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
					
			String fila=fl.readLine();
			String [] arrayfila=fila.split(";");
			
			while ((fila= fl.readLine())!= null) {
				String [] arrayaux=fila.split(";");
				Map<String,Integer> mapa=new TreeMap<>();
				for (int i=1;i<arrayaux.length;i++) {
					mapa.put(arrayfila[i],Integer.parseInt(arrayaux[i]));
					
				}
				parque.put(arrayaux[0], mapa);
				System.out.print(arrayaux[0]);
				System.out.println(mapa);
			}
			
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}

	public void mostrar() {		     
		     
		    	 System.out.println(parque);		
}

	public int numeroVehiculos(String provincia, String vehiculo) {
		
		return parque.get(provincia).get(vehiculo);
	}

	public int totalVehiculosProvincia(String provincia) {	
		
		Map <String,Integer> mapa=parque.get(provincia);
		int suma=0;
		for (String v:mapa.keySet()) {
			suma=suma+mapa.get(v);
		}
		
		return suma;
	}

	public int totalVehiculosTipo(String vehiculo) {
		int suma=0;
		for (String p:parque.keySet()) {
			Map <String,Integer> mapa=parque.get(p);
			suma=suma+mapa.get(vehiculo);
		}
		return suma;
	}

	public int totalParque() {
		int suma=0;
		for (String p:parque.keySet()) {
			Map <String,Integer> mapa=parque.get(p);
			for (String m:mapa.keySet()) {
				suma=suma+mapa.get(m);
			}
		}
		return suma;
	}

	public void grabarFichero(String path)  {
		try (ObjectOutputStream fw=new ObjectOutputStream (new FileOutputStream(path))) {
			
			for (Map.Entry<String, Map<String, Integer>> p: parque.entrySet()) {
				
				
			}
			
			
		} catch (IOException e) {
			
			System.out.println(e.getMessage());
		}
		
	}
}

