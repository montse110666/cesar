package Ejercicio1;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class PruebaAutomovil {

	public static void main(String[] args) {
		
		//Set<Automovil> automovil=new TreeSet<>();
		Set<Automovil> automovil=new HashSet<>();
		Automovil auto1=new Automovil("opel","capri","H6745",2000,Motor.HIBRIDO,150);
		Automovil auto2=new Automovil("seat","berlina","F2014",1900,Motor.DIESEL,70);
		Automovil auto3=new Automovil("renault","ranchera","P4678",2001,Motor.GASOLINA,110);
		Automovil auto4=new Automovil("opel","capri","C4783",2003,Motor.DIESEL,100);
		automovil.add(auto1);
		automovil.add(auto2);
		automovil.add(auto3);
		automovil.add(auto4);
		
		for (Automovil auto: automovil) {
			System.out.println(auto.toString());
		}
		
		
		Automovil [] autos=new Automovil[automovil.size()];
		int i=0;
		for (Automovil auto:automovil) {
			autos[i]=auto;
			i++;
		}		
		
		System.out.println("SIN ORDENAR");
		
		System.out.println(Arrays.toString(autos));
		
		
		CompararMotorPotencia autoCompara=new CompararMotorPotencia();
		Arrays.sort(autos, autoCompara);
		System.out.println("ORDENADOS");
		
		
		System.out.println(Arrays.toString(autos));
		
		
	}

}
