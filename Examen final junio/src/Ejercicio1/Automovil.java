package Ejercicio1;

public class Automovil implements Comparable<Automovil>{
	
	private String marca;
	private String modelo;
	private String matricula;
	private int a�o;
	private Motor tipo;
	private Integer potencia;
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public int getA�o() {
		return a�o;
	}
	public void setA�o(int a�o) {
		this.a�o = a�o;
	}
	public Motor getTipo() {
		return tipo;
	}
	public void setTipo(Motor tipo) {
		this.tipo = tipo;
	}
	public Integer getPotencia() {
		return potencia;
	}
	public void setPotencia(Integer potencia) {
		this.potencia = potencia;
	}
	@Override
	public String toString() {
		return "\nmarca=" + marca + " modelo=" + modelo + ", matricula=" + matricula + ", a�o=" + a�o
				+ ", tipo=" + tipo + ", potencia=" + potencia;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Automovil other = (Automovil) obj;		
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		if (tipo != other.tipo)
			return false;
		return true;
	}
	public Automovil(String marca, String modelo, String matricula, int a�o, Motor tipo, Integer potencia) {
		super();
		this.marca = marca;
		this.modelo = modelo;
		this.matricula = matricula;
		this.a�o = a�o;
		this.tipo = tipo;
		this.potencia = potencia;
	}
	public Automovil() {
		super();
	}
	@Override
	public int compareTo(Automovil arg0) {
		
		if (marca.compareToIgnoreCase(arg0.getMarca())==0) {
			if (modelo.compareToIgnoreCase(arg0.getModelo())==0){
				return tipo.toString().compareTo(arg0.getTipo().toString());
			}
				
			else return modelo.compareToIgnoreCase(arg0.getModelo());
		}
		else return marca.compareToIgnoreCase(arg0.getMarca());
		
	}
	
	

}
