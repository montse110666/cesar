package Ejercicio1;

import java.util.Comparator;

public class CompararMotorPotencia implements Comparator<Automovil> {

	@Override
	public int compare(Automovil arg0, Automovil arg1) {
		if (arg0.getTipo().compareTo(arg1.getTipo())==0) {
			
				return arg0.getPotencia().compareTo(arg1.getPotencia());
			}
			else {
				return arg0.getTipo().toString().compareTo(arg1.getTipo().toString());
			}
		}
	}



