package unidad8;

import java.util.ArrayDeque;
import java.util.Deque;

public class Ejercicio12bis {

	public static void main(String[] args) {
		
		Deque <Integer> pila=new ArrayDeque<>();
		pila.push(7);
		pila.push(6);
		pila.push(2);
		pila.push(9);
		pila.push(5);
		
		System.out.println(pila);
		duplicar(pila);
		
	}

	private static void duplicar(Deque<Integer> pila) {
		
		Deque <Integer> pila1=new ArrayDeque<>(pila);
		System.out.println(pila1);
		while (!pila1.isEmpty()) {
			pila.offer(pila.poll());
			int c=pila1.poll();
			pila.offer(c);
		}
		System.out.println(pila);
	}

}
