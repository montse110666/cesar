package unidad8;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;



public class ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Integer> lista=new LinkedList<>();
		Random r=new Random();
		for (int i=0;i<100;i++) {
			lista.add(r.nextInt(100)+1);			
		}
		
		Iterator<Integer> i= lista.iterator();
		while (i.hasNext()) {
			System.out.print(i.next()+" ");
		}
		System.out.println();
		Set<Integer> conjunto=new HashSet<>(lista);
        
		for (Integer numero:conjunto) {
			
			System.out.print(numero+" ");
		}
		System.err.println();
		
		Set<Integer> conjunto2=new TreeSet<>(lista);
		conjunto2.forEach(n -> System.out.print(n + " "));
		System.err.println();
	}

}
