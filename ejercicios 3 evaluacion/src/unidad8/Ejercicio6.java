package unidad8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Ejercicio6 {

	public static void main(String[] args) {
	
		ArrayList<Integer> lista=new ArrayList<>(Arrays.asList(7,1,7,10,4,1,4,-2,4,21,10,1,4,7,7));
		
		moda(lista);
	
	}

	private static void moda(ArrayList<Integer> lista) {
		Map<Integer,Integer> mapa=new HashMap<>();
		Collections.sort(lista);
		int cont=1;
		for (int i=0;i<lista.size()-1;i++) {
			if (lista.get(i)==lista.get(i+1)){
				cont++;
			}
			else{
				mapa.put(lista.get(i), cont);
				cont=1;
			}
		}
		System.out.println(mapa);
		CalculaModa(mapa);
		int clave=CalculaModa(mapa);
		System.out.println("Lamoda es: "+clave);
		
	}

	private static int CalculaModa(Map<Integer, Integer> mapa) {
		int valor=0;
		int clave=0;
		for (Map.Entry<Integer, Integer> m:mapa.entrySet()) {
			
			if (m.getValue()>valor) {
				clave=m.getKey();
				valor=m.getValue();
			}
		}
		return clave;
	}
}
