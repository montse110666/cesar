package unidad8;

import java.util.ArrayDeque;
import java.util.Deque;

public class Ejercicio12 {

	public static void main(String[] args) {
		
		Deque <Integer> enteros=new ArrayDeque<>();
		
		enteros.push(5);
		enteros.push(9);
		enteros.push(2);
		enteros.push(6);
		enteros.push(7);		
		
		duplicar(enteros);
	}

	private static void duplicar(Deque<Integer> enteros) {
		
		Deque <Integer> pilaFinal=new ArrayDeque<>();
		
		
		for (Integer i:enteros) {
			pilaFinal.push(i);
			pilaFinal.push(i);
		}
		System.out.print(pilaFinal);
	}
  
}
