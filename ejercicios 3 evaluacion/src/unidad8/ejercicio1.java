package unidad8;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Scanner;

public class ejercicio1 {

	public static void main(String[] args) {
				
		Scanner entrada=new Scanner(System.in);
		int cant;
		System.out.println("Dame la cantidad de nombres: ");
		cant=entrada.nextInt();
		Collection<String> lista=new LinkedHashSet<>(); 
		for (int i=0; i<cant; i++) {
			System.out.println("Introduce el nombre: ");
			String nombre=entrada.next();
			if (!lista.contains(nombre)) 
				lista.add(nombre);
			
		}
		System.out.println(lista);
		entrada.close();

	}

}
