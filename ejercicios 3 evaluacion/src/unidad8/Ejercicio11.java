package unidad8;

import java.util.ArrayList;
import java.util.Collections;

public class Ejercicio11 {

	public static void main(String[] args) {
		
		ArrayList <Integer> lista1=new ArrayList<>();
		ArrayList <Integer> lista2=new ArrayList<>();
		lista1.add(2);
		lista1.add(3);
		lista1.add(1);
		lista1.add(5);
		lista1.add(1);
		lista1.add(3);
		
		lista2.add(3);
		lista2.add(6);
		lista2.add(2);
		lista2.add(6);
		lista2.add(1);
		lista2.add(5);
	
		
		Collections.sort(lista1);
		Collections.sort(lista2);
		
		System.out.print(lista1);
		System.out.print(lista2);
		
		quitarRepetidos(lista1);
		quitarRepetidos(lista2);		
		
		
		contarComunes(lista1,lista2);		

	}

	private static void quitarRepetidos(ArrayList<Integer> lista) {
		int j=0;
		for (int i=0;i<lista.size();i++) {
					
			if (lista.get(i)==j) {
				
				j=lista.get(i);
				lista.remove(lista.get(i));
				
			}
		j=lista.get(i);
		}
		System.out.print(lista);
	}

	private static void contarComunes(ArrayList<Integer> lista1, ArrayList<Integer> lista2) {
		int cont=0;
		for (int i=0;i<lista1.size();i++) {
			for (int j=0;j<lista2.size();j++) {
				if (lista1.get(i)==lista2.get(j)){
					cont++;
				}
				
			}
			
		}
		System.out.println("Las dos listas tienen "+ cont+" n�meros repetidos");
		
	}

}
