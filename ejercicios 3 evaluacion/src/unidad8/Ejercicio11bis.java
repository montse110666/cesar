package unidad8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Ejercicio11bis {

	public static void main(String[] args) {
		
		ArrayList<Integer> lista=new ArrayList<>(Arrays.asList(2,4,6,8,1,3,10,20,15,11,15,15,3));
		ArrayList<Integer> lista1=new ArrayList<>(Arrays.asList(3,6,1,8,14,15,6,4,15,5,5));
		
		Collections.sort(lista);
		Collections.sort(lista1);
		System.out.println(lista);
		System.out.println(lista1);
		
		int comunes=contarComunes(lista,lista1);
		System.out.println(comunes);		

	}

	private static int contarComunes(ArrayList<Integer> lista, ArrayList<Integer> lista1) {
		int aux=0;		
		int comunes=0;
		int aux1=0;
		for (Integer l:lista)
			if (l!=aux) {
				aux=l;				
					for (Integer l1:lista1) {
						if (aux==l1 && l1!=aux1){
							comunes++;
							aux1=l1;
		    		}
		    	}		  
		}
		return comunes;
	}

}
