package unidad8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class ejercicio3 {

	public static void main(String[] args) throws IOException {
	
		BufferedReader tc=new BufferedReader(new InputStreamReader(System.in));
		String texto=tc.readLine();
		String [] palabras=texto.split(" ");
		Set<String> repetidas=new TreeSet<>();
		Set<String> sinRepetir=new TreeSet<>();
		for (String palabra:palabras) {
			if (!sinRepetir.add(palabra))
				repetidas.add(palabra);
		}
		sinRepetir.removeAll(repetidas);
		System.out.println("Palabras que no se repiten: "+sinRepetir);
		System.out.println("Palabras que se repiten: "+repetidas);
		
		
		

	}

}
