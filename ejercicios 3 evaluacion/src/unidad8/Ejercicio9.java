package unidad8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Ejercicio9 {

	public static void main(String[] args) {
		
		Map<String,Integer> mapa=new TreeMap<>();
		
		mapa.put("Juan", 33);
		mapa.put("Hugo", 29);
		mapa.put("Ana", 45);
		mapa.put("Luis", 47);
		mapa.put("Mario", 33);
		mapa.put("Rosa", 29);
		mapa.put("Carmen", 33);
		mapa.put("Elena", 59);
		mapa.put("Benito", 33);
		
	    System.out.println(mapa);
		int edad=ValorMenosRepetido(mapa);

	}

	private static int ValorMenosRepetido(Map<String, Integer> mapa) {
		ArrayList<Integer>lista=new ArrayList<>();
		
		for (Map.Entry<String,Integer> m:mapa.entrySet()) {
			lista.add(m.getValue());			
		}
		Collections.sort(lista);
		System.out.println(lista);
		Map<Integer,Integer> mapa2=new TreeMap<>();
		int cont=1;
		for (int i=0;i<lista.size()-1;i++) {
			if (lista.get(i)==lista.get(i+1)) {
				cont++;
			}else {
				mapa2.put(lista.get(i), cont);
				cont=1;
			}
		}
		System.out.println(mapa2);
		int menor=0;
		int menosrep=0;
		for (Map.Entry<Integer,Integer> m:mapa2.entrySet()) {
			
			if (m.getValue()<menor) {
				if (m.getKey()<menosrep || menosrep==0)
				   menosrep=m.getKey();
				
			}else if (menor==0) {
				menor=m.getValue();
			}
		}
		System.out.println("La edad que menosse repite es: "+menosrep);
		return 0;
	}

}
