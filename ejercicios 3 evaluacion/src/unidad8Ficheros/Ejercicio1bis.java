package unidad8Ficheros;

import java.io.File;
import java.util.Scanner;

public class Ejercicio1bis {
	
	static Scanner tc=new Scanner(System.in);

	public static void main(String[] args) {
		
		File file;
		if (args.length==0) {
			System.out.println("Introduce la ruta :");
			file=new File(tc.nextLine());
		}else file=new File(args[0]);
		if (file.exists()) {
			System.out.println("El fichero existe");
			if (file.isDirectory()) {
				System.out.println("Es un directorio de nombre "+file.getName());
				System.out.println("El fichero tiene un tama�o de "+file.list().length+" elementos");
				
								
			}
			else {
				System.out.println("Es un fichero de nombre "+file.getName());
				System.out.println("El fichero tiene un tama�o de "+file.length()+" bytes");
				
			}
			if (file.canExecute())
				System.out.println("El fichero es ejecutable");
			if (file.canRead())
				System.out.println("el fichero es de lectura");
			if (file.canWrite())
				System.out.println("el fichero tiene permiso de escritura");
			
		
		}else System.out.println("El fichero no existe");
		
		
		
		
	}

}
