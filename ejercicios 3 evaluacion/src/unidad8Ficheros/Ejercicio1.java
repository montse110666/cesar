package unidad8Ficheros;

import java.io.File;
import java.util.Scanner;

public class Ejercicio1 {
	static Scanner tc=new Scanner(System.in);

	public static void main(String[] args) {
		File file;
		if (args.length==0) {
			System.out.println("Ruta: ");
			file=new File(tc.nextLine());
		}
		else {
			file=new File(args[0]);
		}
		if (file.exists()) {
			System.out.println("Nombre del fichero: "+file.getName());
			if (file.isFile()) {
				System.out.println("Es un fichero y su tama�o es: "+file.length()+"bytes");
			}
			else { 
				System.out.println("Es un directorio y contiene: "+file.list().length+" elementos");
			}
			System.out.println("Permisos: \n"+"Escritura: "+file.canWrite()+"\nLectura: "+file.canRead()+"\nEjecuci�n: "+file.canExecute());
		}
		else System.out.println("El fichero no exixte");

	}

}
