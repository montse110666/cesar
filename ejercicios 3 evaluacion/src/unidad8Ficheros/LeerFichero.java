package unidad8Ficheros;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LeerFichero {

	public static void main(String[] args) {
		final String nomFichero="C:\\Users\\Montse\\Documents\\pruebaFicheros.txt";
		try {
			FileReader fichero=new FileReader(nomFichero);
			int valor=fichero.read();
			while (valor!=-1) {
				if (valor!=32) System.out.print((char) valor);
				valor=fichero.read();
			}
		}catch (IOException e) {
			System.out.println("Problemas con el E/S"+ e);
			
		}
		

	}

}
