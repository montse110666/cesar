package unidad8Ficheros;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class RutaFichero {
	static Scanner tc=new Scanner(System.in);

	public static void main(String[] args) {
		
		System.out.println("Introduce la ruta del fichero: ");
		String ruta=tc.next();
		System.out.println("Escribe el texto:  ");
		String texto=tc.next();
		escribeFichero(ruta,texto);
		leeFicheroAlreves(ruta);

	}

	private static void leeFicheroAlreves(String ruta) {
		
		try(FileReader fr=new FileReader(ruta);){
			int j=0;
			int cont=0;
			int valor=fr.read();
			
			while (valor!=-1) {
				cont++;
				valor=fr.read();
			}
			char [] arraytexto=new char[cont];
			FileReader fr1=new FileReader(ruta);
			valor=fr1.read();
			while (valor!=-1) {
				arraytexto[j]=(char)valor;
				valor=fr1.read();
				j++;
			}
			for (int i=arraytexto.length-1;i>=0;i--) {
				char caracter=arraytexto[i];
			    if (Character.isUpperCase(caracter)) System.out.print(Character.toLowerCase(caracter));
			    else  System.out.print(Character.toUpperCase(caracter));
			}
			
		}catch(IOException e) {
			System.out.println("Error " + e);
		}
		
	}

	private static void escribeFichero(String ruta, String texto) {
		
		try(FileWriter fw=new FileWriter(ruta);)
		{
			fw.write(texto);
		
		}
		catch (IOException e) {
			System.out.println("Error de E/S "+e);
		}
	}

}
