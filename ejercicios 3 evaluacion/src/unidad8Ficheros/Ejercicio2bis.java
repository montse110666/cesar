package unidad8Ficheros;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;



public class Ejercicio2bis {

	public static void main(String[] args) {
		
		try (FileInputStream file=new FileInputStream("C:\\Users\\Montse\\Documents\\El Quijote UTF-8 (1).txt")){
			System.out.println(file.read());

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		try (BufferedReader fr=new BufferedReader(new InputStreamReader
				(new FileInputStream("C:\\Users\\Montse\\Documents\\El Quijote UTF-8 (1).txt")))){
			System.out.println(fr.read());
		}catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
