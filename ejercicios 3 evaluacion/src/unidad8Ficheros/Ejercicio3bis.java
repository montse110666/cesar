package unidad8Ficheros;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;



public class Ejercicio3bis {
	
	static Scanner tc=new Scanner(System.in);

	public static void main(String[] args) {
		
		File file;
		if (args.length==0) {
			System.out.println("Ruta : ");
			file=new File(tc.nextLine());
		}
		else file=new File(args[0]);
		
		try (FileReader filew=new FileReader(file)){
			int caracter;
			int cont=0;
			while ((caracter=filew.read())!=-1) {
				cont++;
			}
			System.out.println("Eln�mero de caracteres es: "+cont);
			
		

		} catch (FileNotFoundException e) {
	// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
	// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try (BufferedReader fl=new BufferedReader(new InputStreamReader(new FileInputStream(file)))){
			String linea;
			
			int contlineas=0;
			int contpalabras=0;
			while ((linea=fl.readLine())!=null){
				String []arraypalabra=linea.split(" ");
				contpalabras=contpalabras+arraypalabra.length;	
				contlineas++;
			}
		System.out.println("n� de palabras: "+ contpalabras);	
		System.out.println("� de l�neas: "+ contlineas);
		}catch (FileNotFoundException e) {
	
			e.printStackTrace();
		}catch (IOException e) {
			System.out.println(e.getMessage()+e);
		
	}
}
}
