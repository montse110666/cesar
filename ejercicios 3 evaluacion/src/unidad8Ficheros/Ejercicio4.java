package unidad8Ficheros;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Scanner;

public class Ejercicio4 implements Serializable{
	static Scanner tc=new Scanner(System.in);

	public static void main(String[] args) {
		
		File file;
		if (args.length==0) {
			System.out.println("Ruta : ");
			file=new File(tc.nextLine());
		}
		else file=new File(args[0]);
		
		try (FileReader filew=new FileReader(file)){
			int caracter;
			int cont=0;
			while ((caracter=filew.read())!=-1) {
				cont++;
			}
			grabarFichero(cont);
			System.out.println("Eln�mero de caracteres es: "+cont);	
		} catch (FileNotFoundException e) {	
			e.printStackTrace();
		} catch (IOException e) {	
			e.printStackTrace();
		}
		
		try (BufferedReader fl=new BufferedReader(new InputStreamReader(new FileInputStream(file)))){
			String linea;
			
			int contlineas=0;
			int contpalabras=0;
			while ((linea=fl.readLine())!=null){
				String []arraypalabra=linea.split(" ");
				contpalabras=contpalabras+arraypalabra.length;	
				contlineas++;
			}
		grabarfichero(contpalabras,contlineas);
		System.out.println("n� de palabras: "+ contpalabras);	
		System.out.println("� de l�neas: "+ contlineas);
		}catch (FileNotFoundException e) {	
			e.printStackTrace();
		}catch (IOException e) {
			System.out.println(e.getMessage()+e);		
		}
		leerFicheroBinario();

	}

	private static void leerFicheroBinario() {
		File file=new File("C:\\Users\\Montse\\Documents\\lolo.txt");
		try (ObjectInputStream fl=new ObjectInputStream(new FileInputStream(file))){
			int cont=(int) fl.readObject();
			int contpalabras=(int) fl.readObject();
			int contlineas=(int) fl.readObject();
			System.out.println("Eln�mero de caracteres es: "+cont);
			System.out.println("n� de palabras: "+ contpalabras);	
			System.out.println("� de l�neas: "+ contlineas);
		} catch (IOException | ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
	}

	private static void grabarfichero(int contpalabras, int contlineas) {
		File file=new File("C:\\Users\\Montse\\Documents\\lolo.txt");
		try (ObjectOutputStream fw=new ObjectOutputStream(new FileOutputStream(file,true))){
			fw.writeObject(contpalabras);
			fw.writeObject(contlineas);		
	   }catch (IOException e) {
		   System.out.println(e.getMessage());
	   }
	}

	private static void grabarFichero(int cont) {
		File file=new File("C:\\Users\\Montse\\Documents\\lolo.txt");
		try (ObjectOutputStream fw=new ObjectOutputStream(new FileOutputStream(file,true))){
			fw.writeObject(cont);			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}

}
