package fp.daw.exprog20210602;

import java.util.Comparator;

public class ComparatorAlumnos implements Comparator<Alumno>{

	@Override
	public int compare(Alumno arg0, Alumno arg1) {
		if (arg0.getCiclo().compareToIgnoreCase(arg1.getCiclo())==0) {
			if (arg0.getCurso().compareTo(arg1.getCurso())==0) {
				if (arg0.getApellidos().compareToIgnoreCase(arg1.getApellidos())==0) {
					return arg0.getNombre().compareToIgnoreCase(arg1.getNombre());
				}
				else return arg0.getApellidos().compareToIgnoreCase(arg1.getApellidos());
			}
			else return arg0.getCurso().compareTo(arg1.getCurso());
		}
		else return arg0.getCiclo().compareToIgnoreCase(arg1.getCiclo()); 
	}

}
