package fp.daw.exprog20210602;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;


public class Frecuencia {
	
	ArrayList <String> lista=new ArrayList<>();
		
	public Frecuencia(File fichero) {
			
	try (BufferedReader fl=new BufferedReader(new InputStreamReader(new FileInputStream(fichero)))){		
		
			String linea;
			
			while ((linea = fl.readLine()) != null) {
				
				separarEnPalabras(linea);				
			}
			
			
	} catch (FileNotFoundException e) {
		
		e.printStackTrace();
	} catch (IOException e) {
	
		e.printStackTrace();
	}
	consultarFrecuencia(lista);

	}

	private void consultarFrecuencia(ArrayList<String> lista2) {
		Map <String,Integer> frecuenciaPalabras=new TreeMap<>();
		int cont=1;
		for (int i=0;i<lista2.size()-1;i++) {
			if (lista2.get(i).equalsIgnoreCase(lista2.get(i+1))) cont++;	
			else {
				frecuenciaPalabras.put(lista2.get(i), cont);
				cont=1;
			}
		}
		System.out.println(frecuenciaPalabras);
		
	}

	public ArrayList<String> getLista() {
		return lista;
	}

	public void setLista(ArrayList<String> lista) {
		this.lista = lista;
	}

	private void separarEnPalabras(String linea) {
		String [] array=linea.split(" ");
		
		for (int i=0;i<array.length;i++) {
		    lista.add(array[i]);
		}
		Collections.sort(lista);
		
	}
}
