package fp.daw.exprog20210602;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.Set;
import java.util.TreeSet;

public class Ejercicio1 {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		Set<Alumno> alumno=new TreeSet<>();
		Alumno alu1=new Alumno("Pedro","Suarez",LocalDate.of(2000, 02, 20),"daw",1);
		Alumno alu2=new Alumno("Amelia","fernandez",LocalDate.of(2001, 12, 22),"daw",2);
		Alumno alu3=new Alumno("Antonio","Garcia",LocalDate.of(2002, 04, 10),"dam",1);
		Alumno alu4=new Alumno("Jose","fernadez",LocalDate.of(2000, 02, 18),"daw",1);
		Alumno alu5=new Alumno("Ana","Garcia",LocalDate.of(2000, 01, 02),"daw",2);
		alumno.add(alu1);
		alumno.add(alu2);
		alumno.add(alu3);
		alumno.add(alu4);
		alumno.add(alu5);
				
		for (Alumno alu:alumno) {
			System.out.println(alu);
		}
		ComparatorAlumnos aluComp=new ComparatorAlumnos();
		
		Set<Alumno> alumno2=new TreeSet<>(aluComp);
		alumno2.addAll(alumno);
		
		
		System.out.println();
		for (Alumno alu:alumno2) {
			System.out.println(alu);
		}
		
		try (ObjectOutputStream fw=new ObjectOutputStream(new FileOutputStream("alumnos1.dat"))){
			
		for (Alumno alu:alumno) {			
		
			fw.writeObject(alu);
			
		}
		for (Alumno alu:alumno2) {			
			
			fw.writeObject(alu);
			
		}
	
		}catch (IOException e){
			System.out.println("Error "+e);
		}
	System.out.println("lectura del fichero");
		try (ObjectInputStream fl=new ObjectInputStream(new FileInputStream("alumnos1.dat"))){
			Alumno alumn;
			while  ((alumn=(Alumno)fl.readObject())!=null) {				
				System.out.println(alumn.toString());					
			}
			
		} catch ( EOFException e) {
			
			System.out.println(e.getMessage());
		}catch (IOException e) {
			System.out.println(e.getMessage());
		}catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

}
