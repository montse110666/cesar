package fp.daw.exprog20210602.ejercicio3;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Ejercicio3 {
	

		
	public Ejercicio3(Integer n, Queue<Integer> q) {
		
		super();
		
		
	}		
	
	public Ejercicio3() {
		super();
	}

	public static void main(String[] args) {
		Queue <Integer> q=new LinkedList<>(Arrays.asList(2,13,-42,21,4,9,14,-3,11,5));
		System.out.println(q);
		Scanner tc=new Scanner(System.in);
		System.out.println("Dame el n�mero de elementos a invertir: ");
		int n=tc.nextInt();
		
		
		invertir(n,q);
		
	}

	private static void invertir(int n, Queue<Integer> q) {
		
		Deque <Integer> p=new ArrayDeque<>();
		if (q==null || n>q.size()) {
			throw new IllegalArgumentException();
		}
		
		if (n>0) {			
		
			for (int i=0;i<n;i++) {
				p.push(q.poll());			
			}
			
			while (!p.isEmpty()) {
				q.offer(p.pop());
			}
			int r=q.size()-n;
			for (int i=0;i<r;i++) {
				q.offer(q.poll());
			}
			System.out.println(q);
		}
		else System.out.println("La cola no se modifica");
		
		
		
	}

}
