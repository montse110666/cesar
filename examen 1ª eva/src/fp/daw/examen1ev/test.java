package fp.daw.examen1ev;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class test {

	static Scanner tc=new Scanner(System.in);
	static ArrayList <Profesor> profesor=new ArrayList<>();
	static ArrayList<nomina> nomina1=new ArrayList<>();
	
	public static void crearProfesores() {
		System.out.println("Idprofesor: ");
		String Idprofesor=tc.nextLine();
		for (Profesor p:profesor) {
			if (p.getIdProfesor().equalsIgnoreCase(Idprofesor)) {
				System.out.println("Ese Id de profesor ya exixte, introduce otro ");
				System.out.println("Idprofesor: ");
				Idprofesor=tc.next();
			}
		}
		System.out.println("Nombre: ");
		String nombre=tc.nextLine();
		System.out.println("Apellidos: ");
		String apellidos=tc.nextLine();
		System.out.println("Sueldo base: ");
		Double sueldoBase=tc.nextDouble();
		System.out.println("El profesor es Titular o Interino? (T/I): ");
		String profe=tc.next();
		if (profe.equalsIgnoreCase("T")) {
			System.out.println("Dame la antig�edad del profesor entre 1 y 6 trienios: ");
			int trienios=tc.nextInt();
			Profesor profeobj=new ProfesorTitular(Idprofesor,nombre,apellidos,sueldoBase,trienios);
			profesor.add(profeobj);
		}
		else {
			Profesor profeobj=new ProfesorInterino(Idprofesor,nombre,apellidos,sueldoBase);
			profesor.add(profeobj);
		}
		
		
	}

	public static void introducirProfesores() {
		ProfesorInterino pi1=new ProfesorInterino("45221887-K","Jos�","Hern�ndez L�pez",1789.00);
		ProfesorInterino pi2=new ProfesorInterino("72332634-L","Andr�s","Molt� Parra",1200.00);
		ProfesorInterino pi3=new ProfesorInterino("34998128-M","Jos�","R�os Mesa",1800.00);
		ProfesorTitular pt1=new ProfesorTitular("73-K","Juan","P�rez P�rez",1900.00,2);
		ProfesorTitular pt2=new ProfesorTitular("88-L","Alberto","Centa Mota",1800.00,3);
		ProfesorTitular pt3=new ProfesorTitular("81-F","Mar�a","Mota P�rez",1700.00,5);
		profesor.add(pi1);
		profesor.add(pi2);
		profesor.add(pi3);
		profesor.add(pt1);
		profesor.add(pt2);
		profesor.add(pt3);
	}

	public static void imprimirListin() {
		System.out.println("LISTA DE PROFESORES");
		for (Profesor p:profesor) {
			System.out.println(p.toString());
		}
		
			}

	public static void listaNominas() {
		double nomina=0;
		for (Profesor p:profesor) {
			String id=p.getIdProfesor();
			if (p instanceof ProfesorTitular) {
				nomina=(((ProfesorTitular) p).importeNomina());
			}
			else {
				nomina=(((ProfesorInterino) p).importeNomina());
			}
		  nomina1.add(new nomina(id,nomina));
		}
		
	}

	public static void grabarFichero() {
		try {
			ObjectOutputStream obj = new ObjectOutputStream(new FileOutputStream("nominas.txt"));
			obj.writeObject(nomina1);
			obj.close();
		}
		catch (IOException ex) {
		    System.out.println("error de escritura");
	}
    
	
		
	}

	public static void importeTotalNominaProfesorado() {
		try {
			ObjectInputStream obj = new ObjectInputStream(new FileInputStream("nominas.txt"));
			nomina1=(ArrayList<nomina>) obj.readObject();
			obj.close();
			sumaNominas();
		}
		catch (Exception ex) {
			 System.out.println("error de lectura");
		}
		
	}

	private static void sumaNominas() {
		double suma=0;
		for (nomina n:nomina1) {
			suma=suma+n.getNomina();
		}
		System.out.println("La suma de todas las n�minas es: "+suma);
	}

	public static void menu() {
		int opcion;
		do {
			System.out.println("MENU PROFESORES");
		    System.out.println("1- Introducir un nuevo profesor: ");
		    System.out.println("2- Imprimir lista de profesores: ");
		    System.out.println("3- Imprimir la lista de n�minas: ");
		    System.out.println("4- Grabar fichero: ");
		    System.out.println("5- Imprimir suma de n�minas: ");
		    System.out.println("6- Salir ");
		    opcion=tc.nextInt();
		    switch (opcion) {
		    	case 1: {
		    		test.crearProfesores();
		    	}
		    	break;
		    	case 2: {
		    		test.imprimirListin();
		    	}
		    	break;
		    	case 3: {
		    		test.listaNominas();
		    	}
		    	break;
		    	case 4: {
		    		test.grabarFichero();
		    	}
		    	break;
		    	case 5: {
		    		test.importeTotalNominaProfesorado();
		    	}
		    	break;
		    	case 6: {
		    		System.out.println("Salimos del programa");
		    	}
		    	break;
		    }
		}
		    while (opcion != 6);
		    	
		   	    	
		}
		
	}
		
	


