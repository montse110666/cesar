package fp.daw.examen1ev;

public class Profesor {

	private String IdProfesor;
	private String nombre;
	private String apellidos;
	private Double sueldoBase;
	@Override
	public String toString() {
		return "IdProfesor=" + IdProfesor + ", nombre=" + nombre + ", apellidos=" + apellidos
				+ ", sueldoBase=" + sueldoBase;
	}
	public Profesor(String IdProfesor, String nombre, String apellidos, double sueldoBase) {
		super();
		this.IdProfesor = IdProfesor;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.sueldoBase = sueldoBase;
	}
	public String getIdProfesor() {
		return IdProfesor;
	}
	public void setIdProfesor(String IdProfesor) {
		this.IdProfesor = IdProfesor;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public Double getSueldoBase() {
		return sueldoBase;
	}
	public void setSueldoBase(Double sueldoBase) {
		this.sueldoBase = sueldoBase;
	}
}
