package fp.daw.examen1ev;

import java.util.Random;

public class valoresAleatorios {

	public static void main(String[] args) {
		
		Random aleatorio=new Random();
		int [][] tabla=leervalores(aleatorio);
		numerosPares(tabla);

	}

	private static void numerosPares(int[][] tabla2) {
		int sumapares=0;
		for (int i=0; i<5; i++) {
			for (int j=0; j<4; j++) {
				if (tabla2[j][i]%2==0) {
					sumapares+=tabla2[j][i];
					
				}
			
			}
			System.out.println("La suma de los numeros pares de la columna " +i +" es: "+sumapares);
			sumapares=0;
	    }

	}

	private static int[][] leervalores(Random aleat) {
		int suma=0;
		int [][] tabla1=new int[4][5];
		for (int i=0; i<4; i++) {
			for (int j=0; j<5; j++) {
				tabla1[i][j] =aleat.nextInt(11-1)+1;
				suma+=tabla1[i][j];
				System.out.print(tabla1[i][j]+"  ");
			}
			System.out.println("La suma de los numeros de la fila "+ i +" es: "+suma);
			suma=0;
		}
		return tabla1;
	}

}
