package fp.daw.examen1ev;

public class ejercicio2noIf {

	public static void main(String[] args) {
		int a�o1=2000;
		int a�o2=2021;
		int a�o3=3000;
		int a�o4=1956;
		System.out.println(esBisiesto(a�o1));
		System.out.println(esBisiesto(a�o2));
		System.out.println(esBisiesto(a�o3));
		System.out.println(esBisiesto(a�o4));
	}

	private static boolean esBisiesto(int a�o) {
		
		boolean div4=a�o%4==0;
		boolean div100=a�o%100==0;
		boolean div400=a�o%400==0;
		return div4 && (!div100 || div400);
	}

}
