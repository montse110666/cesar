package fp.daw.examen1ev;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class ProdPrincipal {

	static Scanner tc=new Scanner(System.in);
	static ArrayList<productos> lista=new ArrayList<>();

	public static void main(String[] args) {
		
		cargarPredeterminados();
		agregarmenu();
			
		
	}

	private static void agregarmenu() {
		int opcion;
		do {
			System.out.println("MENU DE OPCIONES");
			System.out.println("1- Grabar Fichero");
			System.out.println("2- Agregar producto");
			System.out.println("3- Calcular importe total");
			System.out.println("4- Eliminar producto");
			System.out.println("5- Visualizar productos");
			System.out.println("6- Salir");
			opcion=tc.nextInt();
			switch (opcion) {
			   case 1: {
				   grabarFichero();
			   }
			   break;
			   case 2: {
				   agregarProducto();
			   }
			   break;
			   case 3: {
				   calcularImporteTotal();
			   }
			   break;
			   case 4: {
				   eliminarProducto();
			   }
			   break;
			   case 5: {
				   visualizarProductos();
			   }
			   break;
			   case 6: {
				   System.out.println("Saliendo");
			   }
			   break;
			}
		}
			   while (opcion!=6);
		}
	
			
		
	
	

	private static void visualizarProductos() {
		// TODO Auto-generated method stub
		
	}

	private static void eliminarProducto() {
		System.out.println("Nombre del producto que quiere eliminar: ");
		String nombre=tc.next();
		Iterator it=lista.iterator(); 
		while (it.hasNext()) {
			productos p=(productos) it.next();
			if (p.getNombre().equalsIgnoreCase(nombre)) {
				it.remove();
			}
		}
		visualizar();
	}

	private static void visualizar() {
		for (productos p:lista) {
			System.out.println(p.toString());
		}
		
	}

	private static void calcularImporteTotal() {
		for(productos p:lista) {
			if (p instanceof Perecederos) {
			   System.out.println("Cantidad a comprar: ");
		       int cant=tc.nextInt();
		       double preciofinal=((Perecederos) p).calcular(cant);
		       System.out.println("El producto "+p.getNombre()+" tienen un precio final de:  "+	preciofinal);
		    }
			else {
				 System.out.println("Cantidad a comprar: ");
			     int cant=tc.nextInt();
			     double preciofinal=((NoPerecederos)p).calcular(cant);
			}
		}
	}

	private static void agregarProducto() {
		System.out.println("Nombre: ");
		String nombre=tc.nextLine();
		System.out.println("C�digo: ");
		String codigo=tc.next();
		for (productos p:lista) {
			if (p.getCodigo().equalsIgnoreCase(codigo)) {
				System.out.println("C�digo exixtente, introduce otro ");
				System.out.println("C�digo: ");
				codigo=tc.next();
			}
		}
		System.out.println("Precio: ");
		Double precio=tc.nextDouble();
		System.out.println("Tipo de producto (P/NP) ");
		String tipo=tc.next();
		if (tipo.equalsIgnoreCase("p")) {
			System.out.println("D�as de caducidad: ");
			int diascad=tc.nextInt();
			productos p=new Perecederos(nombre,codigo,precio,diascad);
			lista.add(p);
		}
		else {
			System.out.println("Tipo: ");
			String tipo1=tc.next();
			productos p=new NoPerecederos(nombre,codigo,precio,tipo1);
			lista.add(p);
		}
		
	}

	private static void grabarFichero() {
		try {
			ObjectOutputStream obj=new ObjectOutputStream(new FileOutputStream("productos.txt"));
			obj.writeObject(lista);
			obj.close();
			
		}catch (IOException ex){
			System.out.println("Error de escritura");
		}
	}

	private static void cargarPredeterminados() {
		NoPerecederos atun=new NoPerecederos("Lata Atun","LT",2.34,"Latas");
		lista.add(atun);
		NoPerecederos sopa=new NoPerecederos("Sopa Vegetal","SP",1.24,"Sobres");
		lista.add(sopa);
		NoPerecederos pasta=new NoPerecederos("Pastall","PT",4.34,"Embasado");
		lista.add(pasta);
		Perecederos lubina=new Perecederos("Lubina","LB",16.34,2);
		lista.add(lubina);
		Perecederos tomates=new Perecederos("Tomates","TA",11.44,3);
		lista.add(tomates);
		Perecederos naranjas=new Perecederos("Naranjas","NJ",6.34,1);
		lista.add(naranjas);
		Perecederos tomates1=new Perecederos("Tomates","TM",3.55,4);
		lista.add(tomates1);
		
		
	}

	

}
