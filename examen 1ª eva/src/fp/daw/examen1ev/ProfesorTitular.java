package fp.daw.examen1ev;

public class ProfesorTitular extends Profesor{

	private double compdestino;
	private int antiguedad;
	
	

	public ProfesorTitular(String IdProfesor, String nombre, String apellidos, double sueldoBase, int antiguedad) {
		super(IdProfesor, nombre, apellidos, sueldoBase);
		this.compdestino=500.00;
		this.antiguedad=antiguedad;
	}

	
	@Override
	public String toString() {
		return super.toString() +"complemento=" + compdestino + ", antiguedad=" + antiguedad;
	}





	public int getAntiguedad() {
		return antiguedad;
	}
	public void setAntiguedad(int antiguedad) {
		this.antiguedad = antiguedad;
	}
	public  double importeNomina() {
		double importe=0;
		double [] trienios= {1.01,1.02,1.05,1.06,1.07,1.08};
		importe= (super.getSueldoBase()+compdestino)*trienios[antiguedad-1]/100;
		return importe;
	}
	
}
