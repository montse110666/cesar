package fp.daw.examen1ev;

public class Motor {
	
	@Override
	public String toString() {
		return "Motor [modelo=" + modelo + ", cantkm=" + cantkm + "]";
	}

	private String modelo;
	private int cantkm;
	
	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getCantkm() {
		return cantkm;
	}

	public void setCantkm(int cantkm) {
		this.cantkm = cantkm;
	}

	public Motor(String modelo, int cantkm) {
		super();
		this.modelo = modelo;
		this.cantkm = cantkm;
	}

}
