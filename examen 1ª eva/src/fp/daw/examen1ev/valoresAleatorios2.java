package fp.daw.examen1ev;

import java.util.Random;

public class valoresAleatorios2 {
public static void main(String[] args) {
		
		Random aleatorio=new Random();
		int [][] tabla=leervalores(aleatorio);
		numerosPares(tabla);

	}

	private static void numerosPares(int[][] tabla2) {
		int sumapares=0;
		int [] vectorsumapares=new int[5];
		for (int i=0; i<5; i++) {
			for (int j=0; j<4; j++) {
				if (tabla2[j][i]%2==0) {
					sumapares+=tabla2[j][i];
					
				}
			
			}
			System.out.println("La suma de los numeros pares de la columna " +i +" es: "+sumapares);
			vectorsumapares[i]=sumapares;
			visulaizasumapares(vectorsumapares);
			sumapares=0;
	    }

	}

	private static void visulaizasumapares(int[] vectorsumapar) {
		System.out.println();
		for (int i=0; i<4; i++) {
							
			System.out.print(vectorsumapar[i]+"  ");
		
	}
	}

	private static int[][] leervalores(Random aleat) {
		int suma=0;
		int [] sumafilas=new int[4];
		int [][] tabla1=new int[4][5];
		for (int i=0; i<4; i++) {
			for (int j=0; j<5; j++) {
				tabla1[i][j] =aleat.nextInt(11-1)+1;
				suma+=tabla1[i][j];
				System.out.print(tabla1[i][j]+"  ");
			}
			System.out.println("La suma de los numeros de la fila "+ i +" es: "+suma);
			sumafilas[i]=suma;
			suma=0;
		}
		visualizasumafilas(sumafilas);
		System.out.println();
		return tabla1;
	}

	private static void visualizasumafilas(int[] sumafil) {
		System.out.println();
		for (int i=0; i<4; i++) {
							
			System.out.print(sumafil[i]+"  ");
			
		
	}

}

}