package interfaces;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class PruebaInterfaces extends JFrame {
	
	public PruebaInterfaces() {
		
		Container c=getContentPane();
		setVisible(true);
		setLocationRelativeTo(null);
		setResizable(false);
		setSize(400, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		c.setLayout(new GridLayout (40, 40));
		
		
		JPanel panelColIzq=new JPanel();
		panelColIzq.setLayout(new BorderLayout());
		panelColIzq.setBackground(Color.green);
		c.add(panelColIzq);
		JPanel panelColDcha=new JPanel();
		panelColDcha.setLayout(new BorderLayout());
		panelColDcha.setBackground(Color.red);
		c.add(panelColDcha);
		
		JButton btn1=new JButton("Bot�n 1");
		JButton btn2=new JButton("Bot�n 2");
		JButton btn3=new JButton("Bot�n 3");
		JButton btn4=new JButton("Bot�n 4");
		JButton btn5=new JButton("Bot�n 5");
		JButton btn6=new JButton("Bot�n 6");
		JButton btn7=new JButton("Bot�n 7");
		
		
		
		panelColIzq.add(btn1);
		panelColDcha.add(btn2);
	}

	public static void main(String[] args) {
		
		PruebaInterfaces vista=new PruebaInterfaces();

	}

}
