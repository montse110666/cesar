package interfaces;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Gryd3Columnas extends JFrame implements ActionListener{
	public JLabel lbl;
	public JPanel izquierdo;
	public JPanel central;
	public JPanel derecho;
	public Container c;
	
	public Gryd3Columnas() {		
		
		c=getContentPane();
		c.setLayout(new GridLayout(0,3,10,0));
		
		izquierdo=new JPanel();
		c.add(izquierdo);
		
		central=new JPanel();
		c.add(central);
		
		derecho=new JPanel();
		c.add(derecho);
		
		JButton bt1=new JButton("Rojo");
		bt1.setActionCommand("Rojo");
		bt1.addActionListener(this);
		JButton bt2=new JButton("Azul");
		bt2.addActionListener(this);
		bt2.setActionCommand("Azul");
		JButton bt3=new JButton("Verde");
		bt3.setActionCommand("Verde");		
		bt3.addActionListener(this);
		
		izquierdo.add(bt1);
		central.add(bt2);
		derecho.add(bt3);
		
		setVisible(true);
		setLocationRelativeTo(null);
		setResizable(false);
		setSize(500, 500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		
	}

	public static void main(String[] args) {
		
		Gryd3Columnas vista=new Gryd3Columnas();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		String origen = e.getActionCommand();
		switch (origen) {
			case "Rojo":
				c.setBackground(Color.red);
//				izquierdo.setBackground(Color.red);
//				central.setBackground(Color.red);
//				derecho.setBackground(Color.red);
				break;
			case "Azul":
				c.setBackground(Color.blue);
				//central.setBackground(Color.blue);
				//izquierdo.setBackground(Color.blue);
				//derecho.setBackground(Color.blue);
				break;
			case "Verde":
				c.setBackground(Color.green);
//				derecho.setBackground(Color.green);
//				central.setBackground(Color.green);
//				izquierdo.setBackground(Color.green);
				break;
			
				
		}
	}

}
