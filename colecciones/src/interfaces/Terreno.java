package interfaces;

public class Terreno {

	private String nombre_forma;
	private int medida;
	private int valor;
	private int descuento;
	
	public String getNombre_forma() {
		return nombre_forma;
	}
	public void setNombre_forma(String nombre_forma) {
		this.nombre_forma = nombre_forma;
	}
	public int getMedida() {
		return medida;
	}
	public void setMedida(int medida) {
		this.medida = medida;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public int getDescuento() {
		return descuento;
	}
	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}
	public Terreno(String nombre_forma, int medida, int valor, int descuento) {
		super();
		this.nombre_forma = nombre_forma;
		this.medida = medida;
		this.valor = valor;
		this.descuento = descuento;
	}
	public Terreno() {
		super();
	}
	@Override
	public String toString() {
		return "Terreno: " + nombre_forma + "\nmedida=" + medida+" m2" + "\nvalor: " + valor+" �" + "\ndescuento: "
				+ descuento;
	}
	
	
}
