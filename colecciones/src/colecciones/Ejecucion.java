package colecciones;

import java.util.*;

public class Ejecucion {

	static Scanner tc=new Scanner(System.in);
	
	public static void main(String[] args) {
		
		Queue<Proceso> lista=new LinkedList<>();
		System.out.println("Introduce el n�mero de procesos: ");
		int N=tc.nextInt();
		for (int i=0; i<N;i++) {
			System.out.println("Introduce id: ");
			int id=tc.nextInt();
			System.out.println("Introduce nombre: ");
			String nombre=tc.next();
			Proceso obj=new Proceso(nombre, id);
			lista.offer(obj);
		}
		Queue<Integer> ideal=new LinkedList<>();
		System.out.println("Introduce el orden de ejecuci�n: ");
		
		for (int i=0; i<N;i++) {
			int orden=tc.nextInt();
			ideal.offer(orden);
			
			System.out.print(" ");
		}
			
		System.out.println(lista);
		Proceso proceso;
		int esperado;
		while (!lista.isEmpty()) {
			proceso=lista.poll();
			esperado=ideal.poll();
			while (proceso.getId()!=esperado) {
				lista.offer(proceso);
				proceso=lista.poll();				
			}
			proceso.ejecutar();
		}
	}
	

}
