package colecciones;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Conjuntos {

	static Scanner tc=new Scanner(System.in);
	
	
	public static void main(String[] args) {
		
		Set <Integer> conjunto1=new HashSet<>();
		Set <Integer> conjunto2=new HashSet<>();
		int m;
		int n;
		System.out.println("Introduce n: ");
		n=tc.nextInt();
		System.out.println("Introduce m: ");
		m=tc.nextInt();
		leerNumeros(n,m,conjunto1,conjunto2);
		System.out.println(conjunto1);
		System.out.println(conjunto2);
		comprobarDuplicados(n,m, conjunto1,conjunto2);

	}

	private static void comprobarDuplicados(int n, int m, Set<Integer> conjunto1, Set<Integer> conjunto2) {
		for (Integer i:conjunto1) {
			for (Integer j:conjunto2){
				if (j==i){
					System.out.print(j+" ");
					
				}
			}
		}		
	}	

	private static void leerNumeros(int n, int m, Set <Integer> conjunto1,Set <Integer> conjunto2) {
		System.out.println("Introduce los "+(n+m)+" numeros: ");
		int i=0;
		while (i<(n+m)) {
			if (i<n)
			conjunto1.add(tc.nextInt());
			else conjunto2.add(tc.nextInt());
			i++;
		}		
	}		
}
