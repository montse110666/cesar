package colecciones;

import java.util.HashSet;
import java.util.Set;

public class CuentasUsuarios {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Clientes cl1=new Clientes("Antonio Banderas", "00001", 200000);
		Clientes cl2=new Clientes("Rafael Nadal", "00002", 250000);
		Clientes cl3=new Clientes("Penelope cruz", "00003", 300000);
		Clientes cl4=new Clientes("Julio Iglesias", "00004", 500000);
		
		Set <Clientes> clientesBanco=new HashSet<Clientes>();
		clientesBanco.add(cl1);
		clientesBanco.add(cl2);
		clientesBanco.add(cl3);
		clientesBanco.add(cl4);
		
		for (Clientes clientes : clientesBanco) {
			System.out.println(clientes.getNombre()+" "+clientes.getN_cuenta()+" "+clientes.getSaldo());
			
		}
	}

}
