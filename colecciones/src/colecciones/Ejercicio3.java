package colecciones;

import java.util.LinkedList;
import java.util.Queue;

public class Ejercicio3 {
	
	public static void main(String[] args) {
	
		Queue <Integer> cola=new LinkedList<>();
		
		cola.offer(1);
		cola.offer(2);
		cola.offer(3);
		cola.offer(4);
		cola.offer(5);
		cola.offer(6);
		cola.offer(7);
		cola.offer(8);
		cola.offer(9);
		cola.offer(10);
		
		intercalar(cola);
	}

	private static void intercalar(Queue<Integer> cola) {
		Queue <Integer> cola2=new LinkedList<>();
		
		
		
		int longitud=cola.size();
		longitud=longitud/2;
		for (int i=0;i<longitud;i++){			
			cola2.offer(cola.poll());				
				
			}
		
		System.out.println(cola2);
		System.out.println(cola);
		for (int i=0;i<longitud;i++){
			cola2.offer(cola2.poll());
			cola2.offer(cola.poll());			
			
		}
		System.out.print(cola2);
	}

}
