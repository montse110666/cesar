package colecciones;

import java.util.HashMap;
import java.util.Map;

public class Ejercicio8 {

	public static void main(String[] args) {
		
		Map <String,Integer> mapa=new HashMap<>();
		Map <String,Integer> mapa1=new HashMap<>();
		mapa.put("Maria", 4);
		mapa.put("Luis", 2);
		mapa.put("Paula", 25);
		mapa.put("Andrea", 25);
		mapa.put("Miguel", 15);
		mapa.put("Laura", 19);
		
		mapa1.put("Julia", 4);
		mapa1.put("Luis", 2);
		mapa1.put("Antonio", 25);
		mapa1.put("Andrea", 25);
		mapa1.put("Marian", 15);
		
		
		Map <String,Integer>inters=interseccion(mapa,mapa1);
		System.out.println(inters);
	}

	private static Map<String, Integer> interseccion(Map<String, Integer> mapa, Map<String, Integer> mapa1) {
		Map<String,Integer> inters=new HashMap<>();
		for (Map.Entry<String, Integer> m: mapa.entrySet()){
			String nombre=m.getKey();
			int edad=m.getValue();
			if (mapa1.containsKey(nombre)){
				if (mapa1.get(nombre)==edad)
				   inters.put(nombre, edad);
				
			}
		}
		
		return inters;
	}

}
