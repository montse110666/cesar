package colecciones;

public class Proceso {
	
	private String nombre;
	private int id;
	public Proceso(String nombre, int id) {
		super();
		this.nombre = nombre;
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public void ejecutar() {
		System.out.println("Ejecutando "+nombre);
	}
	@Override
	public String toString() {
		return "Proceso [nombre=" + nombre + ", id=" + id + "]";
	}
}
