package colecciones;


import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;

public class OrdenInverso {

	public static void main(String[] args) {
		
		Deque<Integer> cola=new LinkedList<>();
		Scanner tc=new Scanner(System.in);
		System.out.println("Cuantos n�meros quieres introducir: ");
		int cant=tc.nextInt();
		for (int i=0;i<cant;i++) {			
			int numero=tc.nextInt();
			cola.offer(numero);
		}
		while (!cola.isEmpty()) {
			
			System.out.print(cola.pollLast()+" ");
		}
		
		System.out.println("Fin");

	}

}
