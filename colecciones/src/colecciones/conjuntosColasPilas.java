package colecciones;


import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;


public class conjuntosColasPilas {
	
	static Scanner tc=new Scanner(System.in);

	public static void main(String[] args) {
		
				
			Queue <Integer> conjunto1=new LinkedList<>();
			Queue <Integer> conjunto2=new LinkedList<>();
			int m;
			int n;
			System.out.println("Introduce n: ");
			n=tc.nextInt();
			System.out.println("Introduce m: ");
			m=tc.nextInt();
			leerNumeros(n,m,conjunto1,conjunto2);
			System.out.println(conjunto1);
			System.out.println(conjunto2);
			comprobarDuplicados(n,m, conjunto1,conjunto2);

		}

		private static void comprobarDuplicados(int n, int m, Queue<Integer> conjunto1, Queue<Integer> conjunto2) {
			for (Integer i:conjunto1) {
				for (Integer j:conjunto2){
					if (j==i){
						System.out.print(j+" ");
						
					}
				}
			}		
		}	

		private static void leerNumeros(int n, int m, Queue<Integer> conjunto1,Queue<Integer> conjunto2) {
			System.out.println("Introduce los "+(n+m)+" numeros: ");
			boolean c=false;
			int i=0;
			while (i<(n+m)) {
				if (i<n) {
					
						int num=tc.nextInt();
						compruebaRepetidos(conjunto1,num);					
						if (c==true) conjunto1.offer(num);	
					
				}
				
				else {
					
						int num=tc.nextInt();
						compruebaRepetidos(conjunto2,num);					
						if (c==true) conjunto2.offer(num);
						
					}
				i++;
			}
			}	
		
		private static boolean compruebaRepetidos(Queue<Integer> conjunto, int num) {
		    boolean c=true;
			for (Integer i:conjunto) {
				if (num==i) c=false;
				
				
			}
			
			return c;
		}		
	

		

	}


