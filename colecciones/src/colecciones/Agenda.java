package colecciones;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class Agenda {
static Map<Direccion, Set<Persona>> mapaAgenda = new TreeMap<>();
static Set<Persona> persona=new HashSet<>();
static Set<Persona> persona1=new HashSet<>();
static Set<Persona> persona2=new HashSet<>();
static Set<Persona> persona3=new HashSet<>();
static Scanner tc=new Scanner(System.in);

	public static void main(String[] args) {
		
		
		introducePersonas();
		rellenaMapa();
						
		
		
		System.out.println(mapaAgenda);
		String provincia=leerProvincia();
		mostrarPoblacion(provincia);
		introducirPersona(mapaAgenda);	
		
		
	}

	
	private static void introducirPersona(Map<Direccion, Set<Persona>> mapaAgenda2) {
	    
		
			System.out.println("Introduce la direcci�n: ");
			System.out.println("calle: ");
			String calle=tc.next();
			System.out.println("n�mero: ");
			int num=tc.nextInt();
			System.out.println("poblaci�n: ");
			String pobla=tc.next();
			Direccion dir5=new Direccion(num,calle,pobla);
			boolean existe=false;
			for (Map.Entry<Direccion, Set<Persona>> clave:mapaAgenda2.entrySet()) {
				Direccion dire=clave.getKey();
				Set<Persona> nom=clave.getValue();
				if (((Map<Direccion,Set<Persona>>) clave).containsKey(dir5)) {
					System.out.println("Direcci�n encontrada, introduce la persona: ");
					String persona=tc.next();
					 if (nom.contains(persona)) {
						 System.out.println("Esa persona ya existe en esa direcci�n");
						 existe=true;	
						 		
				     }
					 //else mapaAgenda.put(dir5, nom.add(new Persona(persona)));
				}
			}
				
		}			

	
	private static void rellenaMapa() {
		Direccion dir1=new Direccion(2,"quirinal","Asturias");
		Direccion dir2=new Direccion(5,"quirinal", "Asturias");
		Direccion dir3=new Direccion(21,"pardo","Salamanca");
		Direccion dir4=new Direccion(2,"quirinal","Asturias");
		
		mapaAgenda.put(dir1, persona);		
		mapaAgenda.put(dir2, persona1);		
		mapaAgenda.put(dir3, persona2);			
		mapaAgenda.put(dir4, persona3);
				
	}

	private static void introducePersonas() {
				
		persona.add(new Persona("Angel"));
		persona.add(new Persona("Jose"));
		persona.add(new Persona("Antonio"));
		persona.add(new Persona("montse"));
		persona1.add(new Persona("ana"));
		persona1.add(new Persona("marian"));
		persona1.add(new Persona("pedro"));
		persona1.add(new Persona("luis"));
		persona2.add(new Persona("elena"));
		persona2.add(new Persona("leire"));
		persona2.add(new Persona("alex"));
		persona2.add(new Persona("aroa"));
		persona3.add(new Persona("jone"));
		persona3.add(new Persona("andres"));
		persona3.add(new Persona("hugo"));
	}

	
	private static void mostrarPoblacion(String provincia) {

		for (Map.Entry<Direccion, Set<Persona>> clave:mapaAgenda.entrySet()) {
			Direccion dire=clave.getKey();
			Set<Persona> per=clave.getValue();
		    if (dire.getProvincia().equalsIgnoreCase(provincia)){
			  
			   System.out.println(dire+" "+per);
		   }
		}
		
	}

	private static String leerProvincia() {
		System.out.println("Introduce provincia: ");
		String provincia=tc.next();
		return provincia;
	}


	}
