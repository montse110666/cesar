package colecciones;
public class Direccion implements Comparable<Direccion>{
private int num;
private String calle;
private String provincia;

public String getProvincia() {
return provincia;
}

public void setProvincia(String provincia) {
this.provincia = provincia;
}

public Direccion(int num, String calle, String provincia) {
super();
this.num = num;
this.calle = calle;
this.provincia = provincia;
}


@Override
public String toString() {
	return "Direccion [num=" + num + ", calle=" + calle + ", provincia=" + provincia + "]";
}

public int getNum() {
return num;
}

public void setNum(int num) {
this.num = num;
}

public String getCalle() {
return calle;
}

public void setCalle(String calle) {
this.calle = calle;
}

@Override
public int compareTo(Direccion o) {
	int res=this.num-o.num;
	if (res==0)
		res=this.calle.compareTo(o.calle);
	if (res==0)
		res=this.provincia.compareTo(o.provincia);
	return res;
}

public Direccion(int num, String calle) {
super();
this.num = num;
this.calle = calle;
}

}