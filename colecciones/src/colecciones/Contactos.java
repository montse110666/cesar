package colecciones;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Contactos {
	
	static Scanner tc=new Scanner(System.in);

	public static void main(String[] args) {
		
		Map<String, Set<String>> agenda=new TreeMap<>();
		Set<String> telefonos=new TreeSet<>();
		String opcion;
		do {
			System.out.print("> ");
			opcion=tc.nextLine();
			if (!opcion.contains("buscar:") || !opcion.contains("eliminar:") || !opcion.contains("contactos") ||
				!opcion.contains("salir:")) {
				System.out.println(opcion);
				aņadirNombre(agenda,opcion,telefonos);
			}
			if (opcion.contains("buscar:")	|| opcion.contains("eliminar:")) {
				buscarTelefonos(agenda,telefonos,opcion);
			}
			if (opcion.contains("contactos")){
				System.out.println(agenda);
			}
		
			}	
		while (!opcion.contains("salir:"));
			
		}
	

	private static void buscarTelefonos(Map<String, Set<String>> agenda, Set<String> telefonos, String opcion) {
		String [] arraynombres=opcion.split(":");
		String op=arraynombres[0];
		 System.out.println(op);
		String nombre=arraynombres[1];
		 System.out.println(nombre);
		for (Map.Entry<String, Set<String>> clave:agenda.entrySet()){
			if (((Map<String, Set<String>>) clave).containsKey(nombre)) {
		        System.out.println(op);
				if (op.contains("eliminar")) {
					agenda.remove(clave);
				}
				if (op.contains("buscar"))	{
					
					System.out.print(telefonos);				
				}
					
			}
						
		}	
	}
		
	

	private static void aņadirNombre(Map<String, Set<String>> agenda, String opcion, Set<String> telefonos) {
		String [] arraynombres=opcion.split(":");
		String nombre=arraynombres[0];
		String telefono=arraynombres[1];
		telefonos.add(telefono);
		agenda.put(nombre, telefonos);
		
	}

	
}
